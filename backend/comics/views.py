from django.shortcuts import render
from rest_framework import generics, views
from rest_framework.permissions import IsAuthenticated
from comics.models import Character, Creator, Comic, Favorite
from comics.serializers import CharacterSerializer, CreatorSerializer, ComicSerializer, FavoriteSerializer


class CharacterMixin:
    queryset = Character.objects.all()
    serializer_class = CharacterSerializer


class CreatorMixin:
    queryset = Creator.objects.all()
    serializer_class = CreatorSerializer


class ComicMixin:
    queryset = Comic.objects.all()
    serializer_class = ComicSerializer


class FavoriteMixin:
    permission_classes = [IsAuthenticated]
    lookup_url_kwarg = "user_id"
    lookup_field = "user_id"
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer


class CharacterListAPIView(CharacterMixin, generics.ListAPIView):

    def get_queryset(self):
        qs = super().get_queryset()
        name = self.request.GET.get("name")
        if name:
            return qs.filter(name__icontains=name)
        return qs


class CharacterRetrieveAPIView(CharacterMixin, generics.RetrieveAPIView):
    pass


class CreatorListAPIView(CreatorMixin, generics.ListAPIView):

    def get_queryset(self):
        qs = super().get_queryset()
        name = self.request.GET.get("name")
        if name:
            return qs.filter(name__icontains=name)
        return qs


class CreatorRetrieveAPIView(CreatorMixin, generics.RetrieveAPIView):
    pass


class ComicListAPIView(ComicMixin, generics.ListAPIView):

    def get_queryset(self):
        qs = super().get_queryset()
        title = self.request.GET.get("title")
        if title:
            return qs.filter(title__icontains=title)
        return qs


class ComicRetrieveAPIView(ComicMixin, generics.RetrieveAPIView):
    pass


class FavoriteRetrieveAPIView(FavoriteMixin, generics.RetrieveAPIView):
    pass


class FavoriteCreateAPIView(FavoriteMixin, generics.CreateAPIView):
    pass


class FavoriteUpdateAPIView(FavoriteMixin, generics.UpdateAPIView):
    pass