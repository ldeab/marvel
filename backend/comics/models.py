from django.db import models
from backend import settings


class Character(models.Model):
    api_id = models.CharField("API ID", max_length=255, unique=True)
    name = models.CharField("Nome", max_length=255)
    description = models.TextField("Descrição")
    thumb = models.CharField('Thumb', max_length=255)

    class Meta:
        ordering = ["name"]


class Creator(models.Model):
    api_id = models.CharField("API ID", max_length=255, unique=True)
    name = models.CharField("Name", max_length=255)
    thumb = models.CharField('Thumb', max_length=255)

    class Meta:
        ordering = ["name"]


class Comic(models.Model):
    api_id = models.CharField("API ID", max_length=255, unique=True)
    title = models.CharField("Título", max_length=255)
    description = models.TextField("Descrição")
    pages = models.IntegerField("Páginas")
    date = models.DateField("Data de Modificação")
    thumb = models.CharField('Thumb', max_length=255)
    price = models.FloatField('Preço')
    characters = models.ManyToManyField(Character)
    creators = models.ManyToManyField(Creator)

    class Meta:
        ordering = ["title"]


class Favorite(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, models.CASCADE)
    characters = models.ManyToManyField(Character)
    creators = models.ManyToManyField(Creator)
    comics = models.ManyToManyField(Comic)
    date = models.DateField("Data", auto_now_add=True)

    class Meta:
        ordering = ["-date"]

