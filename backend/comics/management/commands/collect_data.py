from comics.models import Character, Comic, Creator
from django.core.management import base
import requests
import hashlib
import json
from tqdm import tqdm
from datetime import datetime
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    base_url = "http://gateway.marvel.com/v1/public/"
    public_key = "98057f53586baa54f9d57962d020d368"
    private_key = "48fb9c1650051a252bb454b93ec1a75e2daf7a50"
    characters = []
    creators = []
    comics = []

    def search(self, uri, var, offset=0):
        ts = datetime.now().timestamp()
        hs = hashlib.md5(str(ts).encode()
                         + self.private_key.encode()
                         + self.public_key.encode()).hexdigest()
        try:
            r = requests.get(f"{self.base_url}{uri}?apikey={self.public_key}&ts={ts}&hash={hs}&offset={offset}&limit=100")
            if r.status_code == 200:
                result = json.loads(r.content)["data"]
                var += result["results"]
                if result["total"] > len(var):
                    self.search(uri, var, offset+100)
        except:
            print("Skipping some records")

    def handle(self, *args, **options):
        print("*" * 80)
        self.stdout.write('>>> Collecting data...')
        print("*" * 80)

        print("Retrieving Characters...")
        self.search("characters", self.characters)
        for character in tqdm(self.characters, desc="Updating Characters", ncols=100):
            character_data = {
                "api_id": character["id"],
                "name": character["name"],
                "description": character["description"],
                "thumb": f"{character['thumbnail']['path']}.{character['thumbnail']['extension']}"
            }
            try:
                obj = Character.objects.get(api_id=character_data["api_id"])
                for key in character_data.keys():
                    setattr(obj, key, character_data[key])
            except Character.DoesNotExist:
                obj = Character(**character_data)
            obj.save()

        print("Retrieving Creators...")
        self.search("creators", self.creators)
        for creator in tqdm(self.creators, desc="Updating Creators", ncols=100):
            creator_data = {
                "api_id": creator["id"],
                "name": creator["fullName"],
                "thumb": f"{creator['thumbnail']['path']}.{creator['thumbnail']['extension']}"
            }
            try:
                obj = Creator.objects.get(api_id=creator_data["api_id"])
                for key in creator_data.keys():
                    setattr(obj, key, creator_data[key])
            except Creator.DoesNotExist:
                obj = Creator(**creator_data)
            obj.save()

        print("Retrieving Comics...")
        self.search("comics", self.comics)
        for comic in tqdm(self.comics, desc="Updating Comics", ncols=100):
            comic_data = {
                "api_id": comic["id"],
                "title": comic["title"],
                "description": comic["description"] or "",
                "pages": comic["pageCount"],
                "date": next(date["date"].split("T")[0] for date in comic["dates"] if date["type"] == "onsaleDate"),
                "thumb": f"{comic['thumbnail']['path']}.{comic['thumbnail']['extension']}",
                "price": next(price["price"] for price in comic["prices"] if price["type"] == "printPrice"),
            }
            if len(comic_data["date"]) > 10:
                comic_data["date"] = comic_data["date"][1:0]
            if len(comic_data["date"]) == 0:
                comic_data["date"] = "1111-11-11"
            try:
                obj = Comic.objects.get(api_id=comic_data["api_id"])
                for key in comic_data.keys():
                    setattr(obj, key, comic_data[key])
            except Comic.DoesNotExist:
                obj = Comic(**comic_data)
            obj.save()

            if obj:
                for creator in comic["creators"]["items"]:
                    try:
                        api_id = creator["resourceURI"].split("/")[-1]
                        obj.creators.add(Creator.objects.get(api_id=api_id))
                    except Creator.DoesNotExist:
                        pass
                for character in comic["characters"]["items"]:
                    try:
                        api_id = character["resourceURI"].split("/")[-1]
                        obj.characters.add(Character.objects.get(api_id=api_id))
                    except Character.DoesNotExist:
                        pass
