from django.contrib import admin
from django.urls import path
from comics import views

urlpatterns = [
    path('characters/', views.CharacterListAPIView.as_view(), name="character-list"),
    path('characters/<int:pk>/', views.CharacterRetrieveAPIView.as_view(), name="character-retrieve"),
    path('creators/', views.CreatorListAPIView.as_view(), name="creators-list"),
    path('creators/<int:pk>/', views.CreatorRetrieveAPIView.as_view(), name="creators-retrieve"),
    path('comics/', views.ComicListAPIView.as_view(), name="comics-list"),
    path('comics/<int:pk>/', views.ComicRetrieveAPIView.as_view(), name="comics-retrieve"),
    path('favorites/<int:user_id>', views.FavoriteRetrieveAPIView.as_view(), name="favorites-list"),
    path('favorites/add/', views.FavoriteCreateAPIView.as_view(), name="favorites-create"),
    path('favorites/change/<int:user_id>', views.FavoriteUpdateAPIView.as_view(), name="favorites-change"),
]
