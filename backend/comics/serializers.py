from rest_framework import serializers

from comics.models import Character, Creator, Comic, Favorite
from user.models import User
from user.serializers import UserSerializer


class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Character
        fields = "__all__"


class CreatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Creator
        fields = "__all__"


class ComicSerializer(serializers.ModelSerializer):
    characters = CharacterSerializer(many=True, read_only=True)
    creators = CreatorSerializer(many=True, read_only=True)
    class Meta:
        model = Comic
        fields = "__all__"


class FavoriteSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    characters = serializers.PrimaryKeyRelatedField(many=True, queryset=Character.objects.all())
    creators = serializers.PrimaryKeyRelatedField(many=True, queryset=Creator.objects.all())
    comics = serializers.PrimaryKeyRelatedField(many=True, queryset=Comic.objects.all())
    class Meta:
        model = Favorite
        fields = "__all__"

