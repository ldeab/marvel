from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from user.models import User
from user.serializers import UserSerializer


class UserMixin:
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserAPIView(UserMixin, generics.CreateAPIView):
    pass


class UserUpdateAPIView(UserMixin, generics.UpdateAPIView):
    permission_classes  = [IsAuthenticated]


class UserRetrieveAPIView(UserMixin, generics.ListAPIView):
    permission_classes  = [IsAuthenticated]

    def get_queryset(self):
        qs = super(UserRetrieveAPIView, self).get_queryset()
        return qs.filter(email=self.request.user.email)