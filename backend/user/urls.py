from django.contrib import admin
from django.urls import path
from user import views
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('user/create/', views.UserAPIView.as_view(), name="user-create"),
    path('user/retrieve/', views.UserRetrieveAPIView.as_view(), name="user-retrieve"),
    path('user/change/<int:pk>/', views.UserUpdateAPIView.as_view(), name="user-change"),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
