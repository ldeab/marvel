import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../views/Home.vue"
import List from "../views/List"
import Login from "../views/Login.vue"
import Register from "../views/Register.vue"
import Favorites from "../views/Favorites.vue"
import store from "../store"
import Profile from "../views/Profile"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/list",
    name: "List",
    component: List,
    props: true
  },
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/register",
    name: "Register",
    component: Register
  },
  {
    path: "/favorites",
    name: "Favorites",
    component: Favorites,
    meta: {
      require_auth: true
    }
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
    meta: {
      require_auth: true
    }
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.require_auth && !store.state.user) {
    next("/login/?from=" + from.fullPath)
  }
  next()
})

export default router
