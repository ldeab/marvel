import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    access_token: localStorage.getItem("access_token"),
    refresh_token: localStorage.getItem("refresh_token"),
    characters: [],
    creators: [],
    comics: [],
    favorites: {
      characters: [],
      creators: [],
      comics: []
    },
    // base_url: "http://localhost:8000/api/"
    base_url: process.env.VUE_APP_BASE_URL
  },
  mutations: {
    set_characters (state, payload) {
      state.characters = payload
    },
    set_creators (state, payload) {
      state.creators = payload
    },
    set_comics (state, payload) {
      state.comics = payload
    },
    set_favorites (state, payload) {
      state.favorites = payload
    },
    set_user (state, payload) {
      state.user = payload
    },
    set_access_token (state, payload) {
      state.access_token = payload
      payload ? localStorage.setItem("access_token", payload) : localStorage.removeItem("access_token")
    },
    set_refresh_token (state, payload) {
      state.refresh_token = payload
      payload ? localStorage.setItem("refresh_token", payload) : localStorage.removeItem("refresh_token")
    }
  },
  actions: {
    retrieve_characters (ctx, data) {
      const endpoint = data.name ? `characters/?name=${data.name}` : "characters/"
      return new Promise(resolve => {
        Vue.axios({
          url: ctx.state.base_url + endpoint,
          method: "get"
        }).then(r => {
          ctx.commit("set_characters", r.data)
          resolve(true)
        })
      })
    },
    retrieve_creators (ctx, data) {
      const endpoint = data.name ? `creators/?name=${data.name}` : "creators/"
      return new Promise(resolve => {
        Vue.axios({
          url: ctx.state.base_url + endpoint,
          method: "get"
        }).then(r => {
          ctx.commit("set_creators", r.data)
          resolve(true)
        })
      })
    },
    retrieve_comics (ctx, data) {
      const endpoint = data.name ? `comics/?title=${data.title}` : "comics/"
      return new Promise(resolve => {
        Vue.axios({
          url: ctx.state.base_url + endpoint,
          method: "get"
        }).then(r => {
          ctx.commit("set_comics", r.data)
          resolve(true)
        })
      })
    },
    retrieve_favorites (ctx) {
      return new Promise((resolve, reject) => {
        Vue.axios({
          url: ctx.state.base_url + "favorites/" + ctx.state.user.id,
          method: "get",
          headers: { Authorization: "Bearer " + ctx.state.access_token }
        }).catch(e => {
          if (e.response.status === 401) {
            reject(e.response)
          }
          if (e.response.status === 404) {
            ctx.commit("set_favorites", { characters: [], creators: [], comics: [] })
            reject(e.response)
          }
        }).then(r => {
          if (r.status === 200) {
            ctx.commit("set_favorites", r.data)
          }
          resolve(true)
        })
      })
    },
    update_favorites (ctx, data) {
      const i = ctx.state.favorites[data.slug].indexOf(data.item)
      if (i !== -1) {
        ctx.state.favorites[data.slug].splice(i, 1)
      } else {
        ctx.state.favorites[data.slug].push(data.item)
      }
      return new Promise(resolve => {
        Vue.axios({
          url: ctx.state.base_url + "favorites/change/" + ctx.state.user.id,
          method: "put",
          data: {
            user: ctx.state.user.id,
            characters: ctx.state.favorites.characters,
            creators: ctx.state.favorites.creators,
            comics: ctx.state.favorites.comics
          },
          headers: { Authorization: "Bearer " + ctx.state.access_token }
        }).catch(e => {
          if (e.response.status === 401) {
            resolve(false)
          }
        }).then(r => {
          ctx.commit("set_favorites", r.data)
          resolve(true)
        })
      })
    },
    retrieve_user (ctx) {
      return new Promise((resolve) => {
        Vue.axios({
          url: ctx.state.base_url + "user/retrieve/",
          method: "get",
          headers: { Authorization: "Bearer " + ctx.state.access_token }
        }).catch(e => {
          if (e.response.status === 401) {
            resolve(false)
          }
        }).then(r => {
          ctx.commit("set_user", r.data[0])
          resolve(true)
        })
      })
    },
    update_user (ctx, data) {
      return new Promise((resolve, reject) => {
        Vue.axios({
          url: ctx.state.base_url + `user/change/${ctx.state.user.id}/`,
          method: "put",
          data: { ...data, username: data.email },
          headers: { Authorization: "Bearer " + ctx.state.access_token }
        }).catch(e => {
          if (e.response.status in [400, 401]) {
            reject(e.response)
          }
          throw Error("Não foi possível atualizar o usuário")
        }).then(r => {
          if (r.status === 200) {
            ctx.commit("set_user", r.data)
            resolve(true)
          }
        })
      })
    },
    retrieve_tokens (ctx, data) {
      return new Promise(resolve => {
        Vue.axios({
          url: ctx.state.base_url + "token/",
          method: "post",
          data: data
        }).catch(e => {
          if (e.response.data.detail === "No active account found with the given credentials") {
            resolve(false)
          }
        }).then(r => {
          if (r.status === 200) {
            ctx.commit("set_access_token", r.data.access)
            ctx.commit("set_refresh_token", r.data.refresh)
            resolve(true)
          }
        })
      })
    },
    refresh_token (ctx) {
      return new Promise(resolve => {
        Vue.axios({
          url: ctx.state.base_url + "token/refresh/",
          method: "post",
          data: { refresh: ctx.state.refresh_token }
        }).then(r => {
          if (r.status === 200) {
            ctx.commit("set_access_token", r.data.access)
            resolve(r.data.access)
          }
        })
      })
    },
    register (ctx, data) {
      return new Promise((resolve, reject) => {
        Vue.axios({
          url: ctx.state.base_url + "user/create/",
          method: "post",
          data: { ...data, username: data.email }
        }).catch(e => {
          reject(e.response.data)
          throw Error("Usuário não encontrado")
        }).then(r => {
          if (r.status === 201) {
            resolve(true)
          }
        })
      })
    },
    logout (ctx) {
      ctx.commit("set_access_token", null)
      ctx.commit("set_refresh_token", null)
      ctx.commit("set_user", null)
    }
  },
  modules: {}
})
