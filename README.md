# Marvel: Stone Case

## Project setup
```
git clone https://gitlab.com/ldeab/marvel.git
cd marvel/
docker-compose up -d
```

## Retrieve data from marvel api
```
docker-compose exec app bash -c "python backend/manage.py collect_data"
```
